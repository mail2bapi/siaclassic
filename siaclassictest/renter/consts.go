package renter

import (
	"os"

	"gitlab.com/SiaClassic-Foundation/SiaClassic/siaclassictest"
)

// renterTestDir creates a temporary testing directory for a renter test. This
// should only every be called once per test. Otherwise it will delete the
// directory again.
func renterTestDir(testName string) string {
	path := siaclassictest.TestDir("renter", testName)
	if err := os.MkdirAll(path, 0777); err != nil {
		panic(err)
	}
	return path
}
